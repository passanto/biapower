package io.biapower.newmeter;

import static java.lang.String.format;

public class Constants {

    public static final String DEVICE_ID = "s01234";

    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String FIRST_READING_DATE = "2022-03-01 12:01:55";
    public static final String SECOND_READING_DATE = "2022-03-01 12:02:55";
    public static final String THIRD_READING_DATE = "2022-03-01 12:03:55";

    public static final String ERROR_READING_DATE = "2022-13-01 22:01:55";

    public static final String FIRST_READING_PUT = "{\n" +
            "  \"timestamp\": \"" + FIRST_READING_DATE + "\",\n" +
            "  \"device_sn\": \"" + DEVICE_ID + "\",\n" +
            "  \"energy\": 12.12\n" +
            "}";

    public static final String SECOND_READING_PUT = "{\n" +
            "  \"time-stamp\": \"" + SECOND_READING_DATE + "\",\n" +
            "  \"serial\": \"" + DEVICE_ID + "\",\n" +
            "  \"energy\": 22.12\n" +
            "}";

    public static final String THIRD_READING_PUT = "{\n" +
            "  \"timeStamp\": \"" + THIRD_READING_DATE + "\",\n" +
            "  \"device-serial\": \"" + DEVICE_ID + "\",\n" +
            "  \"e\": 32.12\n" +
            "}";

    public static final String ALL_READINGS =
            format("[{\"id\":2,\"timeStamp\":\"%s\",\"energy\":12.12,\"power\":0.0},{\"id\":3,\"timeStamp\":\"%s\",\"energy\":22.12,\"power\":0.16666666666666669},{\"id\":4,\"timeStamp\":\"%s\",\"energy\":32.12,\"power\":0.16666666666666666}]",
                    FIRST_READING_DATE, SECOND_READING_DATE, THIRD_READING_DATE);

    public static final String START_DATE_READINGS =
            format("[{\"id\":4,\"timeStamp\":\"%s\",\"energy\":32.12,\"power\":0.16666666666666666}]",
                    THIRD_READING_DATE);

    public static final String END_DATE_READINGS =
            format("[{\"id\":2,\"timeStamp\":\"%s\",\"energy\":12.12,\"power\":0.0},{\"id\":3,\"timeStamp\":\"%s\",\"energy\":22.12,\"power\":0.16666666666666669}]",
                    FIRST_READING_DATE, SECOND_READING_DATE);

    public static final String INVALID_DATE_READING_PUT = "{\n" +
            "  \"timestamp\": \"" + ERROR_READING_DATE + "\",\n" +
            "  \"device_sn\": \"" + DEVICE_ID + "\",\n" +
            "  \"energy\": 12.12\n" +
            "}";

    public static final String INVALID_PARAMETER_READING_PUT = "{\n" +
            "  \"time\": \"" + FIRST_READING_DATE + "\",\n" +
            "  \"device_sn\": \"" + DEVICE_ID + "\",\n" +
            "  \"energy\": 12.12\n" +
            "}";

}
