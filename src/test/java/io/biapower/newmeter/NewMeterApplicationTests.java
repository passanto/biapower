package io.biapower.newmeter;

import io.biapower.newmeter.controllers.ReadingController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class NewMeterApplicationTests {

	@Autowired
	ReadingController readingController;

	@Test
	void contextLoads() {
		assertThat(readingController).isNotNull();
	}


}
