package io.biapower.newmeter.services;

import io.biapower.newmeter.models.Device;
import io.biapower.newmeter.models.Reading;
import io.biapower.newmeter.repositories.DeviceRepository;
import io.biapower.newmeter.repositories.ReadingRepository;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static io.biapower.newmeter.Constants.DATE_FORMAT;
import static io.biapower.newmeter.Constants.DEVICE_ID;
import static io.biapower.newmeter.Constants.FIRST_READING_DATE;
import static io.biapower.newmeter.services.ReadingService.calculatePower;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ReadingServiceTest {


    @Mock
    private ReadingRepository readingRepository;

    @Mock
    private DeviceRepository deviceRepository;

    @InjectMocks
    private ReadingService readingService;

    @Test
    @Order(1)
    void SaveFirstReading_ReturnsNoPower() {
        LocalDateTime date = LocalDateTime.parse(FIRST_READING_DATE, DateTimeFormatter.ofPattern(DATE_FORMAT));

        Optional<Device> deviceOptional = Optional.empty();

        Device deviceWithoutId = new Device(DEVICE_ID);
        Device deviceWithId = new Device(0, DEVICE_ID);

        Reading readingWithoutId = new Reading(date, deviceWithId, 12.12d, 0d);
        Reading readingWithId = new Reading(0, date, deviceWithId, 12.12d, 0d);

        given(deviceRepository.findDevicesByDeviceId(DEVICE_ID)).willReturn(deviceOptional);
        given(deviceRepository.save(deviceWithoutId)).willReturn(deviceWithId);

        given(readingRepository.findReadingByDevice_DeviceIdOrderByTimeStamp(DEVICE_ID)).willReturn(List.of());
        given(readingRepository.save(readingWithoutId)).willReturn(readingWithId);

        final Reading reading = readingService.saveReading(DEVICE_ID,
                date,
                12.12d);

        assertThat(reading).isEqualTo(readingWithId);
        assertThat(reading.getPower()).isZero();
    }

    @Test
    @Order(2)
    void SaveSecondReading_ReturnsPower() {
        LocalDateTime date = LocalDateTime.parse(FIRST_READING_DATE, DateTimeFormatter.ofPattern(DATE_FORMAT));

        Device deviceWithId = new Device(1, DEVICE_ID);
        Optional<Device> deviceOptional = Optional.of(deviceWithId);


        Reading persistedReading = new Reading(0, date, deviceWithId, 12.12d, 0d);
        double power = calculatePower(12.12d, persistedReading.getEnergy(), date, persistedReading.getTimeStamp());
        Reading readingWithoutId = new Reading(date, deviceWithId, 12.12d, power);
        Reading readingWithId = new Reading(1, date, deviceWithId, 12.12d, power);

        given(deviceRepository.findDevicesByDeviceId(DEVICE_ID)).willReturn(deviceOptional);

        given(readingRepository.findReadingByDevice_DeviceIdOrderByTimeStamp(DEVICE_ID)).willReturn(List.of(persistedReading));
        given(readingRepository.save(readingWithoutId)).willReturn(readingWithId);

        final Reading reading = readingService.saveReading(DEVICE_ID,
                date,
                12.12d);

        assertThat(reading).isEqualTo(readingWithId);
        assertThat(reading.getPower()).isEqualTo(readingWithId.getPower());
    }

    @Test
    @Order(2)
    void GetReadings_ReturnsAll() {
        LocalDateTime date = LocalDateTime.parse(FIRST_READING_DATE, DateTimeFormatter.ofPattern(DATE_FORMAT));

        Device deviceWithId = new Device(1, DEVICE_ID);
        Reading persistedReading = new Reading(0, date, deviceWithId, 12.12d, 0d);

        given(readingService.getReadings(null, null, null)).willReturn(List.of(persistedReading));

        final List<Reading> readings = readingService.getReadings(null,null,null);

        assertThat(readings).isEqualTo(List.of(persistedReading));
        assertThat(readings.get(0).getPower()).isEqualTo(persistedReading.getPower());
    }

}