package io.biapower.newmeter.controllers;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static io.biapower.newmeter.Constants.ALL_READINGS;
import static io.biapower.newmeter.Constants.DEVICE_ID;
import static io.biapower.newmeter.Constants.END_DATE_READINGS;
import static io.biapower.newmeter.Constants.FIRST_READING_PUT;
import static io.biapower.newmeter.Constants.INVALID_DATE_READING_PUT;
import static io.biapower.newmeter.Constants.INVALID_PARAMETER_READING_PUT;
import static io.biapower.newmeter.Constants.SECOND_READING_DATE;
import static io.biapower.newmeter.Constants.SECOND_READING_PUT;
import static io.biapower.newmeter.Constants.START_DATE_READINGS;
import static io.biapower.newmeter.Constants.THIRD_READING_DATE;
import static io.biapower.newmeter.Constants.THIRD_READING_PUT;
import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ReadingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    void GetAllReadings_ReturnsEmpty() throws Exception {
        String url = "/readings";

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    @Order(2)
    void AddFirstReading() throws Exception {
        this.mockMvc.perform(put("/readings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(FIRST_READING_PUT))
                .andDo(print())
                .andExpect(status().isOk());
    }


    @Test
    @Order(3)
    void AddSecondReading() throws Exception {
        this.mockMvc.perform(put("/readings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(SECOND_READING_PUT))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    void AddThirdReading() throws Exception {
        this.mockMvc.perform(put("/readings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(THIRD_READING_PUT))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Order(5)
    void GetAllReadings_ReturnsAll() throws Exception {
        String url = "/readings";

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(ALL_READINGS));
    }

    @Test
    @Order(6)
    void GetAllDevicesByDeviceId_ReturnsAll() throws Exception {
        String url = format("/readings?device-id=%s", DEVICE_ID);

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(ALL_READINGS));
    }

    @Test
    @Order(7)
    void GetAllDevicesByStartDate_ReturnsOne() throws Exception {
        String url = format("/readings?start=%s", THIRD_READING_DATE);

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(START_DATE_READINGS));
    }

    @Test
    @Order(8)
    void GetAllDevicesByEndDate_ReturnsTwo() throws Exception {
        String url = format("/readings?end=%s", SECOND_READING_DATE);

        this.mockMvc.perform(get(url)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(END_DATE_READINGS));
    }

    @Test
    @Order(9)
    void AddInvalidDateReading_Fails() throws Exception {
        this.mockMvc.perform(put("/readings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(INVALID_DATE_READING_PUT))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Order(10)
    void AddInvalidParameterReading_Fails() throws Exception {
        this.mockMvc.perform(put("/readings")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(INVALID_PARAMETER_READING_PUT))
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }


}