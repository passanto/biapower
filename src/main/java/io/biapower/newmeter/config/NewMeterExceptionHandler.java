package io.biapower.newmeter.config;

import io.biapower.newmeter.exceptions.ValidationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.format.DateTimeParseException;

@ControllerAdvice
public class NewMeterExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ValidationException.class, NumberFormatException.class, DateTimeParseException.class})
    protected ResponseEntity<Object> handleNotValidInput(Exception ex, WebRequest request) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
