package io.biapower.newmeter.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "application")
@Data
public class Props {

    private List<String> timeStrings;

    private List<String> serialStrings;

    private List<String> energyStrings;
}
