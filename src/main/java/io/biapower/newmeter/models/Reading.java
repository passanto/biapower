package io.biapower.newmeter.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Data
@Table(name = "readings")
public class Reading {

    public Reading(long id, LocalDateTime timeStamp, Device device, Double energy, Double power) {
        this.id = id;
        this.timeStamp = timeStamp;
        this.device = device;
        this.energy = energy;
        this.power = power;
    }

    public Reading(LocalDateTime timeStamp, Device device, Double energy, Double power) {
        this.timeStamp = timeStamp;
        this.device = device;
        this.energy = energy;
        this.power = power;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "reading_id")
    private long id;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime timeStamp;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "device_id", nullable = false)
    @JsonIgnore
    @ToString.Exclude
    private Device device;

    @NotNull
    private Double energy;

    @NotNull
    private Double power;

}
