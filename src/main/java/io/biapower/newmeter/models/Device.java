package io.biapower.newmeter.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Data
@Table(name = "devices")
public class Device {

    public Device(long id, String deviceId) {
        this.id = id;
        this.deviceId = deviceId;
    }

    public Device(String deviceId) {
        this.deviceId = deviceId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @NotNull
    @Column(name = "device_id")
    private String deviceId;

    @OneToMany(mappedBy = "device")
    @JsonIgnore
    @ToString.Exclude
    private List<Reading> readings = new ArrayList<>();


}
