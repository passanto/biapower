package io.biapower.newmeter.dtos;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class DataPointsDTO {

    private LocalDateTime timeStamp;
    private String deviceSerial;
    private Double energy;


}
