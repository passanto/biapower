package io.biapower.newmeter.repositories;

import io.biapower.newmeter.models.Reading;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ReadingRepository extends CrudRepository<Reading, Long> {


    /*
    this query allows flexibility
    all parameters are optional
     */
    @Query("select r from Reading r " +
            "where (:dateFrom is null or r.timeStamp >= :dateFrom) and " +
            "(:dateTo is null or r.timeStamp <= :dateTo) and " +
            "(:deviceId is null or r.device.deviceId = :deviceId)")
    List<Reading> findAllByDateFromAndDateToAndDeviceId(@Param("dateFrom") LocalDateTime dateFrom,
                                                        @Param("dateTo") LocalDateTime dateTo,
                                                        @Param("deviceId") String deviceId);

    List<Reading> findReadingByDevice_DeviceIdOrderByTimeStamp(String deviceId);
}
