package io.biapower.newmeter.repositories;

import io.biapower.newmeter.models.Device;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DeviceRepository extends CrudRepository<Device, Long> {

    Optional<Device> findDevicesByDeviceId(String deviceId);

}
