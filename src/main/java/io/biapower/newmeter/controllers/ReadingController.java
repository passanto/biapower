package io.biapower.newmeter.controllers;

import io.biapower.newmeter.config.Props;
import io.biapower.newmeter.exceptions.ValidationException;
import io.biapower.newmeter.models.Reading;
import io.biapower.newmeter.services.ReadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/readings")
public class ReadingController {

    private ReadingService readingService;
    private Props props;

    @GetMapping
    public List<Reading> getReadings(@RequestParam(name = "start", required = false) @Valid
                                     @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") final LocalDateTime dateFrom,
                                     @RequestParam(name = "end", required = false) @Valid
                                     @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") final LocalDateTime dateTo,
                                     @RequestParam(name = "device-id", required = false) final String deviceId) {
        return readingService.getReadings(dateFrom, dateTo, deviceId);
    }

    @PutMapping
    public void addReading(@RequestBody final Map<String, String> input) throws ValidationException {
        LocalDateTime timeStamp = getTime(input);
        String deviceSerial = getDeviceSerial(input);
        double energy = getEnergy(input);
        readingService.saveReading(deviceSerial, timeStamp, energy);
    }

    // can throw NumberFormatException
    private double getEnergy(final Map<String, String> input) throws ValidationException {
        var key = props.getEnergyStrings().stream()
                .filter(input::containsKey)
                .findFirst().orElseThrow(() -> new ValidationException("energy reading not found"));
        double v = Double.parseDouble(input.get(key));
        if (v < 0) {
            throw new ValidationException("energy value has to be a positive number");
        }
        return v;
    }

    private String getDeviceSerial(Map<String, String> input) throws ValidationException {
        var key = props.getSerialStrings().stream()
                .filter(input::containsKey)
                .findFirst().orElseThrow(() -> new ValidationException("device serial not found"));
        return input.get(key);
    }

    // can throw DateTimeParseException
    private LocalDateTime getTime(Map<String, String> input) throws ValidationException {
        String key = props.getTimeStrings().stream()
                .filter(input::containsKey)
                .findFirst().orElseThrow(() -> new ValidationException("time stamp not found"));
        return LocalDateTime.parse(input.get(key),
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    @Autowired
    public void setReadingService(ReadingService readingService) {
        this.readingService = readingService;
    }

    @Autowired
    public void setProps(Props props) {
        this.props = props;
    }

}
