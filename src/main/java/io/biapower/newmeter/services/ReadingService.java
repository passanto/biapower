package io.biapower.newmeter.services;

import io.biapower.newmeter.models.Device;
import io.biapower.newmeter.models.Reading;
import io.biapower.newmeter.repositories.DeviceRepository;
import io.biapower.newmeter.repositories.ReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Service
public class ReadingService {

    private ReadingRepository readingRepository;
    private DeviceRepository deviceRepository;

    public Reading saveReading(String deviceSN, LocalDateTime timeStamp, double currentEnergy) {
        var deviceOptional = deviceRepository.findDevicesByDeviceId(deviceSN).or(
                () -> Optional.of(deviceRepository.save(new Device(deviceSN)))
        );
        double power = 0.0;
        Optional<Reading> lastReadingOptional = readingRepository.findReadingByDevice_DeviceIdOrderByTimeStamp(deviceSN)
                .stream().findFirst();
        if (lastReadingOptional.isPresent()) {
            power = calculatePower(currentEnergy, lastReadingOptional.get().getEnergy(), timeStamp, lastReadingOptional.get().getTimeStamp());
        }
        return readingRepository.save(new Reading(timeStamp, deviceOptional.get(), currentEnergy, power));
    }

    public List<Reading> getReadings(LocalDateTime dateFrom, LocalDateTime dateTo, String deviceId) {
        return readingRepository.findAllByDateFromAndDateToAndDeviceId(dateFrom, dateTo, deviceId);
    }

    public static double calculatePower(double currentEnergy, double previousEnergy, LocalDateTime currentTime, LocalDateTime lastTime) {
        // power = energy / time
        int time = (int) ChronoUnit.SECONDS.between(lastTime, currentTime);
        return (currentEnergy - previousEnergy) / time;
    }

    @Autowired
    public void setReadingRepository(ReadingRepository readingRepository) {
        this.readingRepository = readingRepository;
    }

    @Autowired
    public void setDeviceRepository(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

}
