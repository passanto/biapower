package io.biapower.newmeter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewMeterApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewMeterApplication.class, args);
	}

}
