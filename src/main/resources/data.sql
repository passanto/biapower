CREATE TABLE IF NOT EXISTS devices
(
    id        BIGINT AUTO_INCREMENT PRIMARY KEY,
    device_id VARCHAR NOT NULL
);


CREATE TABLE IF NOT EXISTS readings
(
    reading_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    time_stamp TIMESTAMP        NOT NULL,
    device_id  VARCHAR          NOT NULL,
    energy     DOUBLE PRECISION NOT NULL DEFAULT 0,
    power      DOUBLE PRECISION NOT NULL DEFAULT 0,
    FOREIGN KEY (device_id) REFERENCES devices (device_id)
);
CREATE INDEX timestamp_index ON readings (time_stamp);
CREATE INDEX device_index ON readings (device_id);

CREATE SEQUENCE IF NOT EXISTS hibernate_sequence START WITH 1 INCREMENT BY 1;

