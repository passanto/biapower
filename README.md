# BiaPower Test

### Prerequisites  
* Java 11  
* Maven 3  

### Test the project
```$ mvn test```

### Run the project
```$ mvn spring-boot:run```

### H2 in memory database
The console is available at the following addres:  
http://localhost:8080/biapower/h2-console  
username: sa  
password:

<b>Embedded mode</b>:  
A <i>new-metersDB</i> file is being created in the <i>target</i> folder.  
All changes are persisted across restarts.  


### Test the project
The IntelliJ compatible <i>readings.http</i> file, located at the root of the project, can be used to send REST calls to the api


### Assumptions
#### Request structure
The GET requests parameters are not required, this way the API can be used to get reading for:
* a specific interval (both <i>start</i> and <i>stop</i> are provided)
* date from (only <i>start</i> is provided)
* date to (only <i>stop</i> is provided)
* a specific device (only <i>device-id</i> is provided)
* any combination of the above
* all the readings from all the devices (no parameter is provided)  
  
The PUT request accepts the following structure:
```json
{
    "timestamp": Timestamp,
    "device_sn": String,
    "energy": Number
}
```
However, all the parameters names can be configured (application.yml) to accept different string.  
IE:  
```yaml
time-strings:
- timestamp
- time-stamp
- timeStamp
```
This way the application can be refreshed (via the Actuator <i>refresh</i> endpoint) upon adding the new required string.  
A scenario where a new consumer does not support any of the strings above (ie. date), the new required one can be added to the external properties file (in an ideal setup those are managed with Spring Cloud Config).  
Once the actuator refreshes the properies injection, the new string will be supported

#### Validation
* timestamp format: yyyy-MM-dd HH:mm:ss
* energy has to be a double: ie. 3.4

#### Power
The submitter assumes that the first reading (for a specific device id) does not provide enough data to calculate the power:  
  
Given that <b>power = energy / time</b>, the time parameter cannot be calculated since there is no "previous" date time.  
  
However, from the second reading the elapsed time can be calculated as follow:  
* the second reading date time, minus the first reading date time gives the elapsed time (in seconds)
* the mandatory energy parameter is used in conjunction with the above duration

#### Kafka Streams
The current industry standard for Kafka streaming is using Avro Confluent registry to expose the ser/des schemas.  
Spring Kafka producers and listeners make use of these schemas to perform the serialization/deserialization.